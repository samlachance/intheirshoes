require_relative '../../lib/twitter_feed'

RSpec.describe 'Twitter Feed Class' do

  let(:feed) { TwitterFeed.new }
  let(:user_id) { 2240775598 }

  before do
    feed.authenticate!
  end

  it 'authenticates with twitter' do
    expect(feed.client.verify_credentials).to be_a(Twitter::User)
  end

  it 'looks up users' do
    expect(feed.lookup(user_id).id).to eq(user_id)
  end

  it 'pulls followees of a user' do
    user_name = "samlachance"

    expect(feed.pull_followees("samlachance")).to be_a(Array)
  end

  it 'pulls user tweets from a time period' do
    expect(feed.fetch_tweets(user_id)).to be_a(Array)
  end

  it 'loads all tweets into feed' do
    individual_count = 5
    users = [2240775598, 396238794, 14561327]

    feed.load_tweets(users, individual_count)

    total_count = users.count * individual_count 
    
    expect(feed.tweets.count).to eq(total_count)
  end

  it 'sorts tweets by time' do
    individual_count = 5
    users = [2240775598, 396238794, 14561327]

    feed.load_tweets(users, individual_count)

    feed.sort_tweets!

    expect(feed.tweets[0].created_at).to be > feed.tweets[1].created_at
  end
end
