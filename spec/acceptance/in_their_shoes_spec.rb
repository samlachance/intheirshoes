require_relative '../../app.rb'
require 'rack/test'

def app
  InTheirShoes
end

RSpec.describe 'In Their Shoes' do
  include Rack::Test::Methods

  it 'serves the root route' do
    get '/'
    expect(last_response.status).to eq(200)
  end
end