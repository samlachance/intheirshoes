require 'twitter'
require 'dotenv/load'

class TwitterFeed
  attr_accessor :client, :tweets

  def initialize
    self.tweets = []
  end

  def authenticate
    Twitter::REST::Client.new do |config|
      config.consumer_key        = ENV['TWITTER_CONSUMER_KEY']
      config.consumer_secret     = ENV['TWITTER_CONSUMER_SECRET']
      config.access_token        = ENV['TWITTER_ACCESS_TOKEN']
      config.access_token_secret = ENV['TWITTER_ACCESS_TOKEN_SECRET']
    end
  end

  def authenticate!
    self.client = authenticate
  end

  def lookup(user_id)
    self.client.user(user_id)
  end

  def pull_followees(user_name)
    self.client.friend_ids("samlachance").to_h[:ids]
  end

  def fetch_tweets(user_id, count =  20)
    self.client.user_timeline(user_id, count: count)
  end

  def load_tweets(users, count = 20)
    users.each do |user|
      self.tweets.push(*fetch_tweets(user, count))
    end
  end

  def sort_tweets!
    self.tweets.sort_by! { |t| t.created_at }.reverse!
  end
end
